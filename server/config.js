const API_VERSION = "v1";
const IP_SERVER = "localhost";
const PORT_DB = 27017;
const DB_NAME = "console";

module.exports = {
  API_VERSION,
  IP_SERVER,
  PORT_DB,
  DB_NAME,
};
