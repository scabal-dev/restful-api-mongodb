const mongoose = require("mongoose");
const app = require("./app");
const PORT_SERVER = process.env.PORT || 3977; //PORT_SERVER = PUERTO DE SERVIDOR
const { API_VERSION, IP_SERVER, PORT_DB, DB_NAME } = require("./config"); //PORT_DB = PUERTO DE BASE DE DATOS

mongoose.set("useFindAndModify", false);

mongoose.connect(
  `mongodb://${IP_SERVER}:${PORT_DB}/${DB_NAME}`,
  { useNewUrlParser: true, useUnifiedTopology: true },
  (error, result) => {
    if (error) {
      throw error;
    } else {
      console.log("La conexion a la base de datos es correcta.");

      app.listen(PORT_SERVER, () => {
        console.log("#####################");
        console.log("###### API REST #####");
        console.log("#####################");
        console.log(`http:${IP_SERVER}:${PORT_SERVER}/api/${API_VERSION}/`);
      });
    }
  }
);
